#include "Client.h"


Client::Client()
{
	id = 0;
	width = 100;
	height = 100;
	impulse = 0;
	mass = 80;
	x = 300;
	y = 800;
	runSpeed = 900;
	maxSpeed = 2000;
	lives = 3;
	timeToDie = 0;
	run = true;
}

Client::Client(int id) {
	this->id = id;
	// defaults
	this->width = 100;
	this->height = 100;
	this->impulse = 0;
	this->mass = 80;
	this->x = 300;
	this->y = 800;
	this->runSpeed = 900;
	this->maxSpeed = 2000;
	this->lives = 3;
	this->run = true;
}

Client *Client::operator=(const Client & other){
	id = other.id;
	x = other.x;
	y = other.y;
	width = other.width;
	height = other.height;
	impulse = other.impulse;
	mass = other.mass;
	lives = other.lives;
	runSpeed = other.runSpeed;
	maxSpeed = other.maxSpeed;
	run = other.run;
	return this;
}

Client::Client(const Client & other){
	*this=other;
}
