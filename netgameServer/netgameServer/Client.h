#pragma once

#include <windows.h>

#include <QVector>



class Client
{
public:
	int id, mass;

	double x, y, runSpeed, maxSpeed, impulse;
	int width, height, lives;
	int timeToDie;
	bool run;
	Client *operator=(const Client &other);
	Client();
	Client(int);
	Client(const Client &other);

	void update(double);
};

struct streamPipeData {
	SOCKET generalSock, updateSock;
	int id;
	sockaddr_in genAddr;
	sockaddr_in updAddr;
};

