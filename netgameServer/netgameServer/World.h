#pragma once

#include "database.h"
#include "Client.h"
#include "QTime"
#include <qstringlist.h>
#include "qdebug.h"

struct Object {
	double x, y;
	int width, height;
};

struct Pair {
	Object top, bottom;
};

class World
{
public:
	int ceil;
	int floor;
	int length;
	int height;
	float gravity;
	int ground[4];
	int sky[4];

	QMap <int, Client> players;
	
	QList<Object> objects;

	void setLength(int);

	void registerPlayer(Client);

	void unregisterPlayer(Client);

	void update(double);
	QList<QString> tracks;
	QString track1, track2, track3, track4,track5,track6,track7,track8,track9,track10;
	bool hitTest(Client, Object);
	void applyGravity(Client &, double);
	void applyImpulse(Client &, double);
	void updatePlayer(Client &, double);
	bool createObject(double, double, int, int);
	void generateBottomSeq(int, int, int);
	void generateTopSeq(int, int, int);
	void generatePairSeq(int, int, int);
	int getRandomHeight();
	int getRandomFreeSpace();
	Pair buildPair(double, int, Pair);
	Pair buildPair(double, int);
	int random(int, int);
	void build();
	int getTrack();
	QString pack();
	
	World();
};
