﻿#include <QtCore/QCoreApplication>
#include <QTextCodec>
#include "QDebug"
#include <qmutex.h>
#include <Windows.h>
#include "Winsock.h"
#include <iostream>
#include <cstring>
#include <QMap>
#include <QString>
#include <QStringList>

#include <qmap.h>
#include "World.h"
#include "Client.h"
#include "database.h"
#include <ctime>

#pragma comment ( lib, "ws2_32.lib" )
using namespace std;

// from client
#define CONNECT 0
#define JUMP 1
#define SWITCH 2
#define QUIT 3
#define PING 4

// to client
#define READY 10
#define UPDATE 20
#define INFO 30
#define NEWPLAYER 40
#define GETPLAYERS 50
#define LOSTPLAYER 60

QMutex mapLock;
QMap <int,Client> clients;
QString formatMess(QMap<int, Client> clients, int id, QString cmd);
DWORD WINAPI createThreadClient (LPVOID param);
DWORD WINAPI worldUpdate(LPVOID param);
QVector<streamPipeData> clientsUpd;

HANDLE hThread;
QVector<int> ids;
World world;
int track;

int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);
	setlocale(LC_CTYPE, "UTF-8");
	QTextCodec * codec= QTextCodec::codecForName("UTF-8");
	QTextCodec::setCodecForTr(codec);
	QTextCodec::setCodecForCStrings(codec);
	QTextCodec::setCodecForLocale(codec);
	
	// проверка наличия уже запущенного сервера
	HANDLE hMutex = CreateMutex(NULL, FALSE, L"Is process here?");
	if (GetLastError() == ERROR_ALREADY_EXISTS) return 0;
	printf("Server started.\n");
	// Инициализация WinSock
	WORD wVersionRequested = MAKEWORD (2,2);
	WSADATA wsaData;
	int err = WSAStartup(wVersionRequested, &wsaData);
	if (err !=0) printf("WSAStartup error: %d\n",WSAGetLastError());
	else		 printf("WinSock intializing\n");

	// Открытие сокетов
	SOCKET generalSock = socket(PF_INET, SOCK_DGRAM, 0);
	if (generalSock == INVALID_SOCKET) { printf("Socket error: %d\n",WSAGetLastError());	WSACleanup(); }
	SOCKET updateSock = socket(PF_INET, SOCK_DGRAM, 0);
	if (generalSock == INVALID_SOCKET) { printf("Socket error: %d\n",WSAGetLastError());	WSACleanup(); }

	// Связывание сокетов 
	sockaddr_in generalSockAddr;
	generalSockAddr.sin_family=PF_INET;
	generalSockAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	generalSockAddr.sin_port=htons(55000);

	if (bind(generalSock,(sockaddr *) &generalSockAddr, sizeof(generalSockAddr))==SOCKET_ERROR) { printf("bind error: %d\n",WSAGetLastError());	closesocket(generalSock);}
	// сокет рассылки обновлений
	sockaddr_in updateSockAddr;
	updateSockAddr = generalSockAddr;
	updateSockAddr.sin_port = htons(66000);

	if (bind(updateSock,(sockaddr *) &updateSockAddr, sizeof(updateSockAddr))==SOCKET_ERROR) { printf("bind error: %d\n",WSAGetLastError()); closesocket(updateSock);}
	
	bool running = true; //выход из цикла
	QString buffString;	 //	QString буфер для преобразований
	QStringList mass;	 //	QStringList буфер для преобразований
	int bsize = 0;		 //	int размер принятых данных
	int tmpID = 0;		 // int временный айди
	char buff[1024];	 // char [1024] буфер для преобразований	

	world = World();
	track = world.getTrack();
	printf("World initialized, building started..%d\n", track);
	//world.build();
	printf("World buided\n");
	/*QString str = world.pack();
	for (int i = 0; i < str.length(); i += 60) {
		qDebug() << (i+60)/ 60 << "/" << ceil((double)str.length() / 60) << " > " << str.mid(i, 60);
	}*/
	

	HANDLE worldThread = CreateThread(NULL, 0, worldUpdate, NULL, NULL, NULL);
	// Обработка присланных пактов
	while(running) {
		sockaddr_in client_addr;	
		int client_addr_size = sizeof(client_addr);
		client_addr.sin_addr.s_addr = inet_addr("0");
		client_addr.sin_port=htons(0);

		bsize = recvfrom(generalSock, buff, sizeof(buff) - 1,0, (sockaddr *) &client_addr, &client_addr_size);
		if (bsize == SOCKET_ERROR)	printf("recvfrom() error: %d\n",WSAGetLastError());	
		buffString = QString::fromAscii(buff,bsize);
		mass = buffString.split(" ");

		if(!clients.contains(mass[1].toInt())) {
			printf("General connection from %s port %d ..", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));	 qDebug() << "with ID:" << mass[1];
			QString toConnect = QString("0:") + mass[1];
			qDebug() << toConnect;
			strcpy(buff, toConnect.toUtf8());
			sendto(generalSock, &buff[0], toConnect.length(), 0, (sockaddr *)&client_addr, sizeof(client_addr));
		}
		switch(mass[0].toInt()) {

		case CONNECT: {
			if (!clients.contains(mass[1].toInt())) {
				tmpID = mass[1].toInt();
				//sendto(generalSock,&buff[0],bsize,0,(sockaddr *)&client_addr, sizeof(client_addr));

				streamPipeData clientData;
				clientData.generalSock = generalSock;
				clientData.updateSock = updateSock;
				clientData.genAddr = client_addr;
				clientData.id = tmpID;

				Client tmpClient;
				bool connect = false;
				while(!connect) {
					bsize = recvfrom(updateSock, buff, sizeof(buff) - 1,0, (sockaddr *) &client_addr, &client_addr_size);
					if (bsize == SOCKET_ERROR)												
						printf("recvfrom() error: %d\n",WSAGetLastError());							
					buffString = QString::fromAscii(buff,bsize);
					mass = buffString.split(" ");
					if(tmpID == mass[1].toInt())
						connect = true;
				}
				tmpClient = Client(tmpID);
				printf("Update connection from %s, port %d ..", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port)); qDebug() << "with ID:" << mass[1];
				clientData.updAddr = client_addr;

				/* NEW PLAYER CMD*/
				for (int i = 0; i < clientsUpd.size(); i++ ) {
					if(clientsUpd[i].id != mass[1].toInt()) {
						QString out = formatMess(clients, mass[1].toInt(), "40");
						qDebug() << "to client: "<< QString::number(clientsUpd[i].id) << out;
						char b[1024]; strcpy(b, out.toUtf8());
						// посылка датаграммы клиенту
						sendto(updateSock, &b[0], out.length(), 0, (sockaddr *)&clientsUpd[i].updAddr, sizeof(clientsUpd[i].updAddr));
					}
				}
				mapLock.lock();
				clients.insert(tmpClient.id,tmpClient);
				clientsUpd.append(clientData);
				mapLock.unlock();

				//создание потока обслуживания для клиента и запись данных клиента
				if(clients.count() == 1)
					hThread = CreateThread(NULL, 0, createThreadClient, (LPVOID)&clientData, NULL, NULL);

				//Клиент успешно подрубился
				QString out = QString::number(CONNECT) + ":" + QString::number(track);
				strcpy(buff, out.toUtf8());
				sendto(updateSock, &buff[0], out.length(), 0, (sockaddr *)&clientData.updAddr, sizeof(clientData.updAddr));
				qDebug() << "New thread created!";

				/* GETPLAYERS*/
			//	out = formatMess(clients, tmpID, QString::number(GETPLAYERS)); strcpy(buff, out.toUtf8());
			//	sendto(updateSock, &buff[0], out.length(), 0, (sockaddr *)&clientData.updAddr, sizeof(clientData.updAddr));
				/**/

				tmpID = 0;
			}
					  } break;

		case QUIT: {

				   } break;
		default :
			qDebug() << "Bug <_<";
			break;

		}
	}
	return a.exec();
}

void timeOut(int curId) {
 QMap<int,Client>::iterator currentClient = clients.begin();
 while(currentClient != clients.end()){
  if(currentClient.value().id == curId) {
   currentClient->timeToDie = 0;
  }
  if(currentClient.value().id != curId){
   currentClient->timeToDie++;
  }
  if(currentClient.value().timeToDie == 500) {
   qDebug() << "Client with id:"<< currentClient.value().id << " is died... :((((";
   ids.append(currentClient.value().id);
  }
  //qDebug() << curId << " - " << currentClient.value().timeToDie;
  currentClient++;
 }

 for (int i = 0; i < ids.count();i++)
  if(clients.contains(ids[i])) {
   clients.remove(ids[i]);
   
   qDebug() << "clients cout:" << clients.count();
  }

  for(int i = 0; i < ids.count();i++) {
	for(int j = 0; j < clientsUpd.count(); j++){
		if(clientsUpd[j].id == ids[i]) {
			clientsUpd.remove(clientsUpd.lastIndexOf(clientsUpd[j]));
		}
	}
}
  ids.clear();
}

DWORD WINAPI worldUpdate (LPVOID param) {
	double dt = 0.016;
	QMap<int, Client>::iterator it;
	while (true) {
		mapLock.lock();
		it = clients.begin();
		while (it != clients.end()) {
			world.updatePlayer(it.value(), dt);
			++it;
		}
		mapLock.unlock();
		Sleep(8);
	}
	return 0;
}


DWORD WINAPI createThreadClient (LPVOID param) {

	streamPipeData data = *(streamPipeData *)param;
	QMap<int, Client>::iterator cl;
	QStringList mess;				QString buffString;
	char buff[1024];				int bsize = 0;
	int ID = data.id;				int client_addr_size = sizeof(data.updAddr);
	sockaddr_in ADDR_FOR_RECFROM;	bool isOk = false;

	Client *clientPtr;
	int clientId;

	isOk = clients.count();
	while(isOk) {

		mapLock.lock();
		int client_addr_size = sizeof(ADDR_FOR_RECFROM);
		bsize = recvfrom(data.updateSock, buff, sizeof(buff) - 1,0, (sockaddr *) &ADDR_FOR_RECFROM, &client_addr_size);
		buffString = QString::fromAscii(buff, bsize);
		mess = buffString.split(" ");
		strcpy(buff, buffString.toUtf8().data());

		//function to remove client without ping
		timeOut(mess[1].toInt());
		QList<int> keys;
		bool done = false;
		int clientId = mess[1].toInt();
		clientPtr = &clients[clientId];

		isOk = clients.count();
		if(isOk) {
			switch(mess[0].toInt())	{
			case PING:	{
				//printf("CMD: PING port: %d :",ntohs(ADDR_FOR_RECFROM.sin_port)); qDebug()<< "mess from id:" << mess[1];
				QString out;
				if (clientPtr->lives == 0) {
					out = formatMess(clients, clientId, mess[0]);
					/*clients.remove(clientId);
					ids.remove(ids.lastIndexOf(clientId));*/
				} else {
					out = formatMess(clients, clientId, mess[0]);
				}

				char b[1024];
				strcpy(b, out.toUtf8());
				sendto(data.updateSock, &b[0], out.length(), 0, (sockaddr *)&ADDR_FOR_RECFROM, sizeof(ADDR_FOR_RECFROM));
						} break;

			case JUMP:	{
				//printf("CMD: JUMP port: %d :",ntohs(ADDR_FOR_RECFROM.sin_port)); qDebug()<< "mess from id:" << mess[1];
				(&clients[clientId])->impulse = world.gravity * 80 * 1.4;
						} break;

			case SWITCH: {
				//printf("CMD: SWITCH port: %d :",ntohs(ADDR_FOR_RECFROM.sin_port)); qDebug()<< "mess from id:" << mess[1];
				if ((&clients[clientId])->run) {
					(&clients[clientId])->run = false;
				} else {
					(&clients[clientId])->run = true;
				}
						 } break;

			case NEWPLAYER: {
				for(int i = 0; i < clientsUpd.size(); i++) {
					if(clientsUpd[i].id == mess[2].toInt()) {
						QString out = formatMess(clients, clientId, "50"); char b[1024]; strcpy(b, out.toUtf8());
						sendto(clientsUpd[i].updateSock, &b[0], out.length(), 0, (sockaddr *)&clientsUpd[i].updAddr, sizeof(clientsUpd[i]).updAddr);
						qDebug() << "CLient here";					
					}
				}
							} break;

			case INFO: {
				QString out = formatMess(clients, clientId, mess[0]);
				qDebug() << "to client: "<< QString::number(clientId)  << out;
				char b[1024];
				strcpy(b, out.toUtf8());
				sendto(data.updateSock, &b[0], out.length(), 0, (sockaddr *)&ADDR_FOR_RECFROM, sizeof(ADDR_FOR_RECFROM));
					   } break;
			}
		}
		mess[1] = "!!";
		mapLock.unlock();
	}
	CloseHandle(hThread);
	closesocket(data.generalSock);
	closesocket(data.updateSock);
	return NULL;
}

QString formatMess(QMap<int, Client> clients, int id, QString cmd) {
	QString out;
	QMap<int, Client>::iterator cl;
	Client *clientPtr = &clients[id];

	switch (cmd.toInt()) {
	case PING: { 
		out = cmd + QString(":") 
			+ QString::number(id) + " " 
			+ QString::number(clientPtr->x) + " " 
			+ QString::number(clientPtr->y) + " " 
			+ QString::number(clientPtr->runSpeed) + " " 
			+ QString::number(clientPtr->lives) +";";

		cl = clients.begin(); 
		while(cl != clients.end()) {
			if(cl.value().id != id) {
				out += QString::number(cl.value().id) + " " + QString::number(cl.value().x) + " " + QString::number(cl.value().y) + ";";
			}
			cl++;
		}
			   } break;

	case INFO: {
		out = cmd + QString(":") 
			+ QString::number(clientPtr->id) + " "
			+ QString::number(clientPtr->runSpeed) + " "
			+ QString::number(clientPtr->lives);

			   } break;

	case NEWPLAYER: {
		out = cmd;
		out += QString(":") + QString::number(id);
					} break;

	case GETPLAYERS: {

		out = cmd + QString(":") + QString::number(id);
		cl = clients.begin();
		while(cl != clients.end()) {
			if(!cl.value().id == id && cl != clients.end() - 1) 
				out += QString::number(cl.value().id) + QString(";");
			if(!cl.value().id == id && cl == clients.end() - 1)
				out += QString::number(cl.value().id);
			cl++;
		}
		qDebug() << " GETPLAYERS :" <<out;
					 } break;
	}
	return out;
}

bool operator==(streamPipeData a, streamPipeData b) {
	return a.id == b.id;
}